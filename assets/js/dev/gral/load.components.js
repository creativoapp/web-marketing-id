$(document).ready(function(){

    var appLoad = {};

    //Nivo Slider :: Home
	$('#is-comp-slider').nivoSlider({
	    effect: 'fade',
	    animSpeed: 500,                 
	    pauseTime: 5000,
	    directionNav: false,
	    controlNav: true
	});

	//Carrousel :: Home
	$('.is-featured-tours').owlCarousel({
		loop: true,
		margin: 20,
		nav: false,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:4
			}
		}
	});

	//Carrousel :: Home/Reviews 
	$('.is-owl-reviews').owlCarousel({
		loop: true,
		margin: 30,
		nav: false,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:3
			}
		}
	});

	//Contact acordeon
	$('.is-questions .is-question').on('click', function(){

		$('.is-questions .is-answer').slideUp('last').removeClass('is-default');
		$(this).siblings('.is-answer').slideDown('last').addClass('is-default');

	});

	//Wowjs
	new WOW().init();

});