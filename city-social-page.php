<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view-servicedetail">
    
    <div class="is-header-service">
        <div class="container">
            <div class="columns">

                <div class="column is-two-thirds">
                    <h1>Redes Sociales en <?=$city_page['name'];?></h1>
                    <div class="is-bread">
                        <a href="/">Web Marketing ID</a>
                        <i class="fas fa-arrow-right"></i>
                        <a href="/<?=$city_page['url'];?>/marketing-digital">Servicios</a>
                        <i class="fas fa-arrow-right"></i>
                        <span>Redes Sociales</span>
                    </div>

                    <p class="is-pr-big">Las Redes Sociales son una parte importante del Marketing Digital ya que influye en la toma de decisión de los consumidores. En Web Marketing ID <?=$city_page['name'];?> contamos con amplia experiencia en la administración de redes sociales para empresas en los diferentes canales.</p>
                </div>
                <div class="column is-one-third">
                    <img src="<?=_IMG.'seo-ilustracion.png';?>" class="is-img-big">
                </div>

            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="columns is-multiline">

            <div class="column is-one-third">
                <img src="<?=_IMG.'mac-book.png';?>">
            </div>

            <div class="column is-two-thirds">
                <h2>Administración de Redes Sociales en <?=$city_page['name'];?>.</h2>
                <p class="is-pr-medium">Nos dedicamos a la Administración de Cuentas Empresariales en las Redes Sociales con los conocimientos necesarios para poder crear atractivas ideas de Marketing dentro de ellas.</p>
                <p class="is-pr-medium">No solo nos dedicamos a la creación de contenido, sino también, como en el caso de Facebook, podemos crear Anuncios Patrocinados dentro de la Red Social, con las características que se necesitan para que lo vea realmente el Cliente que nos interesa.</p>
                <p class="is-pr-medium">Lo Interesante de los Anuncios Patrocinados de Facebook es que puedes segmentar mucho más tu público objetivo, ya que dispones de información de Edad, lugar donde viven los Usuarios, gustos y hábitos, así como Estado Civil, entre otros...</p>
                <p class="is-pr-medium">Es muy Importante que La Página Web de tu negocio en <?=$city_page['name'];?> esté conectada a las Redes Sociales, desde donde los Usuarios podrán ver Críticas y opiniones sobre tu Producto o Servicio.</p>
            </div>

            <div class="column is-half is-listed">
                <div class="is-green">
                    <h3>Redes Sociales en México</h3>

                    <ul class="is-clearfix">
                    <?php foreach($cities as $city) { ?>
                    <li><a href="/<?php echo $city->{'url'}; ?>/redes-sociales"><i class="fas fa-map-marker-alt"></i> <?php echo $city->{'text'}; ?></a></li>
                    <?php } ?>
                    </ul>

                </div>
            </div>

            <div class="column is-half is-listed">
                <h4>Me interesa este servicio</h4>
                <?php include('components/form-service.php'); ?>
            </div>

        </div>
    </div>
    
</section>

<?php require __DIR__ . '/components/footer.php'; ?>