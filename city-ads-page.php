<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view-servicedetail">
    
    <div class="is-header-service">
        <div class="container">
            <div class="columns">

                <div class="column is-two-thirds">
                    <h1>Publicidad en Google <?=$city_page['name'];?></h1>
                    <div class="is-bread">
                        <a href="/">Web Marketing ID</a>
                        <i class="fas fa-arrow-right"></i>
                        <a href="/<?=$city_page['url'];?>/marketing-digital">Servicios</a>
                        <i class="fas fa-arrow-right"></i>
                        <span>Publicidad en Google</span>
                    </div>

                    <p class="is-pr-big">Lleva tu Sitio Web a los mejores resultados de Google en <?=$city_page['name'];?> rápido y fácil con El Pago Por Clic de Google Ads haciendo que las ventas incrementen rápidamente.</p>
                </div>
                <div class="column is-one-third">
                    <img src="<?=_IMG.'seo-ilustracion.png';?>" class="is-img-big">
                </div>

            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="columns is-multiline">

            <div class="column is-one-third">
                <img src="<?=_IMG.'mac-book.png';?>">
            </div>

            <div class="column is-two-thirds">
                <h2>Publicidad pagada en Google <?=$city_page['name'];?>.</h2>
                <p class="is-pr-big">La manera más rápida y efectiva de que tu negocio en <?=$city_page['name'];?> aparezca en los primeros lugares de los resultados de una búsqueda en Internet es el Pago Por Clic y sobretodo Google Ads que son los anuncios patrocinados del mayor buscador de Internet "Google"</p>
            </div>

            <div class="column is-half is-listed">
                <h3>¿Cuáles son los Beneficios de usar Google Ads en <?=$city_page['name'];?>?</h3>
                <p class="is-pr-big">Con los anuncios patrocinados en Google tú decides cuánto quieres gastar; puedes fijar un presupuesto máximo en cada una de las campañas que pongas en marcha y el tope de gasto diario/mensual.</p>
                <p class="is-pr-big">Tienes la opción de fijar el momento y la hora que se lanzará el anuncio, y de dejarlo desactivado cuando desees.</p>

                <div class="is-item">
                    <h4><i class="fas fa-mouse-pointer"></i> Pago por click</h4>
                    <p>Pagas únicamente cuando alguien da clic en tu anuncio, no cuando lo ven.</p>
                </div>

                <div class="is-item">
                    <h4><i class="fas fa-skiing"></i> Es rápido</h4>
                    <p>us anuncios estarán rápidamente posicionados en los primeros resultados del buscador de Google.</p>
                </div>

                <div class="is-item">
                    <h4><i class="far fa-check-square"></i> Resultado eficaz</h4>
                    <p>Tus anuncios aparecerán a aquellos usuarios que estén interesados en tú producto.</p>
                </div>


            </div>

            <div class="column is-half is-listed">
                <div class="is-green">
                    <h3>Publicidad en México</h3>

                    <ul class="is-clearfix">
                    <?php foreach($cities as $city) { ?>
                    <li><a href="/<?php echo $city->{'url'}; ?>/publicidad-google"><i class="fas fa-map-marker-alt"></i> <?php echo $city->{'text'}; ?></a></li>
                    <?php } ?>
                    </ul>

                </div>


                <h4>Me interesa este servicio</h4>
                <?php include('components/form-service.php'); ?>
            </div>

        </div>
    </div>
    
</section>

<?php require __DIR__ . '/components/footer.php'; ?>