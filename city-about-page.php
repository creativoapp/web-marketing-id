<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-about">
    <div class="container">
        <div class="columns is-multiline">

            <div class="column is-full">
                <h1>Web Marketing ID <?=$city_page['name'];?></h1>
                <p class="is-pr-big">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            </div>

            <div class="column is-half">
                <p class="is-pr-medium">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
            </div>
            <div class="column is-half">
                <p class="is-pr-medium">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
            </div>

        </div>
    </div>

    <!--MISION-VISION-->
    <div class="is-vision">
        <div class="container">
            <div class="columns">

                <div class="column is-half">
                    <h2>Visión</h2>
                    <p class="is-pr-big">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore. </p>
                    <img src="<?=_IMG.'vision-ilustracion.png';?>" class="is-img-spaced is-img-small">
                </div>

                <div class="column is-half">
                    <h2>Misión</h2>
                    <p class="is-pr-big">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore. </p>
                    <img src="<?=_IMG.'vision-ilustracion.png';?>" class="is-img-spaced is-img-small">
                </div>

            </div>
        </div>
    </div>


    <!--FEATURES-->
    <div class="container">
        <div class="columns">

            <div class="column is-one-third is-card">
                <div>
                    <h4><i class="fas fa-headphones"></i> Soporte</h4>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore.</p>
                </div>
            </div>

            <div class="column is-one-third is-card">
                <div>
                    <h4><i class="fas fa-mug-hot"></i> Equipo Creativo</h4>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore.</p>
                </div>
            </div>

            <div class="column is-one-third is-card">
                <div>
                    <h4><i class="far fa-thumbs-up"></i> Calidad</h4>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore.</p>
                </div>
            </div>

        </div>
    </div>

</section>

<?php require __DIR__ . '/components/footer.php'; ?>