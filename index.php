<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view-home">
    <div class="is-comp-slider nivoSlider" id="is-comp-slider">
        <!--<a href="/seo"><img src="<?=_IMG.'seo-wmi-banner.jpg'; ?>" title="<?=$sliderCaptions['seo'];?>"></a>
        <a href="/desarrollo"><img src="<?=_IMG.'desarrollo-wmi-banner.jpg'; ?>" title="<?=$sliderCaptions['dev'];?>"></a>-->
        
        <img src="<?=_IMG.'paginas-web-en-mex.jpg';?>">
        <img src="<?=_IMG.'posicionamiento-web-en-mexico.jpg';?>">
        <img src="<?=_IMG.'social-media-en-mexico.jpg';?>">

    </div>

    <!--SERVICIOS-->
    <div class="container">
        <div class="columns is-multiline">

            <div class="column is-full is-overview">
                <h1>Agencia de Marketing Digital en México</h1>
                <p>Somos una Agencia de Marketing digital con presencia en todo México. Nos enfocamos en crear, desarrollar e implementar soluciones integrales y eficientes que se orienten a los objetivos de tu empresa.</p>
                <p>Analizamos cuáles son las necesidades de tu empresa y con ello creamos un plan para el desarrollo de tu marca en internet, ya sea con la creación de un nuevo Sitio Web o el rediseño de uno ya existente, así como el desarrollo del Posicionamiento Web del mismo para estar en los mejores lugares de los resultados de búsqueda de Google.</p>
            </div>

            <div class="column is-one-third is-card">
                <div>
                    <h3><i class="fas fa-search-location"></i> <a href="/seo">Posicionamiento Web</a></h3>
                    <p>Con el Posicionamiento Web o SEO <small>(Search Engine Optimization)</small> llevamos a tu Página Web a las mejores posiciones en los resultados de búsqueda <small>(Google)</small> a través de actividades de Marketing Digital.</p>
                </div>
            </div>

            <div class="column is-one-third is-card">
                <div>
                    <h3><i class="fas fa-chart-line"></i> <a href="/publicidad-google">Publicidad en Google</a></h3>
                    <p>Lleva tu Sitio Web a los mejores resultados de Google rápido y fácil con El Pago Por Clic de Google Ads haciendo que las ventas incrementen rápidamente.</p>
                </div>
            </div>

            <div class="column is-one-third is-card">
                <div>
                    <h3><i class="fas fa-laptop-code"></i> <a href="/paginas-web">Páginas Web</a></h3>
                    <p>El diseño y desarrollo de Páginas Web es algo Importante y sobretodo es el primer paso para tener presencia en Internet. Diseñamos Páginas Web con las características necesarias para su Posicionamiento.</p>
                </div>
            </div>

            <div class="column is-one-third is-card">
                <div>
                    <h3><i class="fas fa-pencil-ruler"></i> <a href="/diseno-grafico">Diseño Gráfico</a></h3>
                    <p>Diseña tu Sitio Web para ganar! El diseño y desarrollo de Páginas Web es muy Importante y sobretodo es el primer paso para tener presencia en Internet. Diseñamos Páginas Web con las características necesarias para su Posicionamiento Web y la generación de Prospectos.</p>
                </div>
            </div>

            <div class="column is-one-third is-card">
                <div>
                    <h3><i class="far fa-comments"></i> <a href="/redes-sociales">Redes Sociales</a></h3>
                    <p>Hoy en día es Necesario tener presencia en todos los medios posibles, y las Redes Sociales están a la vanguardia. Ayudamos con la creación del perfil de la empresa en Redes Sociales, así como su administración y creación de contenido.</p>
                </div>
            </div>

        </div>
    </div>

    <!--CONTACTO-->
    <div class="is-we-contact">
        <div class="container">
            <div class="columns is-multiline">

                <div class="column is-full">
                    <h3>¿Tienes un Proyecto?</h3>
                    <p>Hagámoslo ya!. Proporciónanos tu información para contactarte y ayudarte con tu proyecto.</p>

                    <div class="is-custom">
                        <input type="text" id="h_inpEmail" class="is-first" placeholder="Escribe aquí tu correo">
                        <input type="text" id="h_inpNumber" placeholder="Escribe aquí tu telefono">
                        <button class="is-last"><i class="fas fa-fax"></i> CONTÁCTAME</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!--SEO DEFINICION-->
    <div class="container">
        <div class="columns">

            <div class="column is-half">
                <h3>¿Qué es el Posicionamiento Web?</h3>
                <p class="is-pr-big">El Posicinamiento Web también conocido como SEO tiene por objetivo "Posicionar" una Página Web por encima de las demás (competencia) dentro de los resultados de los Motores de búsqueda como Google, Yahoo, Bing, entre otros.</p>
                <p class="is-pr-big">Esto se hace mejorando el texto del mismo sitio web con palabras claves específicas y creando links de calidad que dirijan a nuestro sitio al cual le estamos realizando las labores de Posicionamiento Web, entre otras labores.</p>
            </div>
            <div class="column is-half">
                <img src="<?=_IMG.'seo-ilustracion.png';?>" class="is-img-medium is-img-spaced">
            </div>

        </div>
    </div>
    <br><br><br>
    <!--MARKETING DEFINICION-->
    <div class="container">
        <div class="columns">

            <div class="column is-half">
                <img src="<?=_IMG.'dev-ilustracion.png';?>" class="is-img-medium is-img-spaced">
            </div>
            <div class="column is-half">
                <h3>¿Qué es el Marketing Digital?</h3>
                <p class="is-pr-medium">El Marketing Digital, es un conjunto de estrategias que realizadas correctamente pueden guiar a un próspero desenlace de una Empresa en Internet. Son varias estrategias y diversas entre sí las que se pueden usar en el Marketing de Internet, entre las cuales están el Posicionamiento Web (SEO), Pago Por Clic, Diseño Web especializado y las Redes sociales.</p>
                <p class="is-pr-medium">El Marketing Digital hoy en día es primordial para todas las empresas, ya que cada vez más consumidores están inmersos en la Red buscando cosas que necesitan porque se les hace más fácil que salir o están distrayéndose en las Redes Sociales, en donde hablan de sus marcas favoritas y los lugares o servicios que les han gustado</p>
                <p class="is-pr-medium">Contáctenos para que le digamos cuál es el Plan de Marketing en Internet que mejor le conviene!</p>
            </div>        

        </div>
    </div>

    <!--CLIENTES-->
    <div class="is-clients">
        <div class="container">
            <div class="columns is-multiline">

                <div class="column is-full">
                    <h4>Ellos ya confiraron en Nostros!</h4>
                </div>

                <div class="column is-one-fifth is-item">
                    <img src="<?=_IMG.'clients/at.png';?>" class="is-img-big is-img-centered">
                </div>
                <div class="column is-one-fifth is-item">
                    <img src="<?=_IMG.'clients/dgyh.png';?>" class="is-img-big is-img-centered">
                </div>
                <div class="column is-one-fifth is-item">
                    <img src="<?=_IMG.'clients/rm-snorkeling.png';?>" class="is-img-big is-img-centered">
                </div>
                <div class="column is-one-fifth is-item">
                    <img src="<?=_IMG.'clients/sundec.png';?>" class="is-img-big is-img-centered">
                </div>
                <div class="column is-one-fifth is-item">
                    <img src="<?=_IMG.'clients/mcr-cancun.png';?>" class="is-img-big is-img-centered">
                </div>

                <div class="column is-one-fifth is-item">
                    <img src="<?=_IMG.'clients/at.png';?>" class="is-img-big is-img-centered">
                </div>
                <div class="column is-one-fifth is-item">
                    <img src="<?=_IMG.'clients/dgyh.png';?>" class="is-img-big is-img-centered">
                </div>
                <div class="column is-one-fifth is-item">
                    <img src="<?=_IMG.'clients/rm-snorkeling.png';?>" class="is-img-big is-img-centered">
                </div>
                <div class="column is-one-fifth is-item">
                    <img src="<?=_IMG.'clients/sundec.png';?>" class="is-img-big is-img-centered">
                </div>
                <div class="column is-one-fifth is-item">
                    <img src="<?=_IMG.'clients/mcr-cancun.png';?>" class="is-img-big is-img-centered">
                </div>
                

            </div>
        </div>
    </div>

    <!--CIUDADES-->
    <div class="container is-locations">
        <div class="columns">

            <div class="column is-two-fifths">
                <img src="<?=_IMG.'location-ilustracion.png';?>">
            </div>
            <div class="column">
                <h3>Marketing Digital en México</h3>
                <p class="is-pr-medium">Web Marketing ID te lleva servicios de Marketing Digital a los principales destinos de México y constantemente abriendo nuevas ciudades.</p>

                <ul class="is-clearfix">
                    <?php foreach($cities as $city) { ?>
                    <li><a href="/<?php echo $city->{'url'}; ?>"><i class="fas fa-map-marker-alt"></i> <?php echo $city->{'text'}; ?></a></li>
                    <?php } ?>
                </ul>

            </div>

        </div>
    </div>

</section>

<?php require __DIR__ . '/components/footer.php'; ?>