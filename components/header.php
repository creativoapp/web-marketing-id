<?php 
    require('app/config.php'); 

    //Cities 404
    if($pageLayout != 'default' && $city_page['name'] == null) { header('Location:/404'); }

    $metas = $seoObj->metas($pageUri, $pageLayout, 'mx', $city_page);

?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    
    <title><?=$metas['title'];?></title>
    <meta name="keywords" content=""/>
    <meta name="description" content="<?=$metas['description'];?>"/>
	
	<meta name="viewport" content="width=device-width, user-scalable=no" />

	<link rel="shortcut icon" type="image/png" href="<?=_IMG.'favicon.ico';?>"/>
	
	<!-- STYLES -->
	<link rel="stylesheet" href="<?=_CSS.'bulma.min.css';?>">
    <link rel="stylesheet" href="<?=_CSS.'components.min.css';?>">
    <link rel="stylesheet" href="<?=_CSS.'wmi.min.css';?>">

    <script src="https://kit.fontawesome.com/94233a6903.js" crossorigin="anonymous"></script>

	<!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>
    <div class="is-top-header">
        <div class="container">
            <div class="columns">

                <div class="column is-one-fifth is-lang">
                    <a href="/en/" class="is-clearfix"><img src="<?=_IMG.'en-lang.png';?>"> <span>English</span></a>
                </div>

                <div class="column is-one-fifth is-contact">
                    <strong><i class="fas fa-phone"></i> Llamános</strong>
                    <a href="tel:9982527702">+52 (998) 252 7702</a>
                </div>

                <div class="column is-one-fifth is-contact">
                    <strong><i class="fab fa-whatsapp"></i> Escríbenos</strong>
                    <a href="tel:9982527702">(998) 252 7702</a>
                </div>

                <div class="column is-one-fifth is-contact">
                    <strong><i class="fas fa-envelope"></i> Contáctanos</strong>
                    <a href="mailto:contacto@webmarketingid.com">contacto@webmarketingid.com</a>
                </div>

                <div class="column is-one-fifth is-social">
                    <a href="https://www.facebook.com/WebMarketingID" target="_blank" class="is-facebook" title="Síguenos en Facebook"><i class="fab fa-facebook"></i></a>
                    <a href="https://twitter.com/WebMarketingID" target="_blank" class="is-twitter" title="Síguenos en Twitter"><i class="fab fa-twitter"></i></a>
                    <a href="#" target="_blank" class="is-instagram" title="Síguenos en Instagram"><i class="fab fa-instagram"></i></a>
                </div>

            </div>
        </div>
    </div>
    
    <header class="is-header">
        <div class="container">
            <div class="columns">

                <div class="column is-one-third is-brand">
                    <a href="/"><img src="<?=_IMG.'web-marketing-id.png';?>"></a>
                </div>

                <div class="column is-two-thirds is-menu">
                    <ul>
                        <li><a href="<?=$cityUriExtend == '' ? '/' : '/'.$cityUriExtend; ?>">Inicio</a></li>
                        <li><a href="<?=$cityUriExtend == '' ? '/agencia-de-marketing-digital' : '/'.$cityUriExtend.'/agencia-de-marketing-digital'; ?>">Nosotros</a></li>
                        <li>
                            <a href="<?=$cityUriExtend == '' ? '/marketing-digital' : '/'.$cityUriExtend.'/marketing-digital';?>">Servicios</a>
                            <ul class="is-level">
                                <li><a href="<?=$cityUriExtend == '' ? '/seo' : '/'.$cityUriExtend.'/seo';?>"><i class="fas fa-search-location"></i> Posicionamiento Web</a></li>
                                <li><a href="<?=$cityUriExtend == '' ? '/publicidad-google' : '/'.$cityUriExtend.'/publicidad-google';?>"><i class="fas fa-chart-line"></i> Publicidad en Google</a></li>
                                <li><a href="<?=$cityUriExtend == '' ? '/paginas-web' : '/'.$cityUriExtend.'/paginas-web';?>"><i class="fas fa-laptop-code"></i> Páginas Web</a></li>
                                <li><a href="<?=$cityUriExtend == '' ? '/diseno-grafico' : '/'.$cityUriExtend.'/diseno-grafico';?>"><i class="fas fa-pencil-ruler"></i> Diseño Gráfico</a></li>
                                <li><a href="<?=$cityUriExtend == '' ? '/redes-sociales' : '/'.$cityUriExtend.'/redes-sociales';?>"><i class="far fa-comments"></i> Redes Sociales</a></li>
                            </ul>
                        </li>
                        <?php if($pageLayout == 'default') { ?>
                        <li><a href="/clientes">Clientes</a></li>
                        <?php } ?>
                        <li><a href="/ubicaciones">Ubicaciones</a></li>
                        <li><a href="<?=$cityUriExtend == '' ? '/contacto' : '/'.$cityUriExtend.'/contacto';?>">Contacto</a></li>
                        <li><a href="/blog/">Blog</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </header>