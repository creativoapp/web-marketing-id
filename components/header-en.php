<?php require('../app/config.php'); ?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    
    <title>Agencia de Marketing Digital en México | Web Marketing Id</title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
	
	<meta name="viewport" content="width=device-width, user-scalable=no" />

	<link rel="shortcut icon" type="image/png" href="<?=_IMG.'favicon.ico';?>"/>
	
	<!-- STYLES -->
	<link rel="stylesheet" href="<?=_CSS.'bulma.min.css';?>">
    <link rel="stylesheet" href="<?=_CSS.'components.min.css';?>">
    <link rel="stylesheet" href="<?=_CSS.'wmi.min.css';?>">

    <script src="https://kit.fontawesome.com/94233a6903.js" crossorigin="anonymous"></script>

	<!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>
    <div class="is-top-header">
        <div class="container">
            <div class="columns">

                <div class="column is-one-fifth is-lang">
                    <a href="/" class="is-clearfix"><img src="<?=_IMG.'es-lang.png';?>"> <span>Español</span></a>
                </div>

                <div class="column is-one-fifth is-contact">
                    <strong><i class="fas fa-phone"></i> Call us</strong>
                    <a href="tel:9982527702">+52 (998) 252 7702</a>
                </div>

                <div class="column is-one-fifth is-contact">
                    <strong><i class="fab fa-whatsapp"></i> Write us</strong>
                    <a href="tel:9982527702">(998) 252 7702</a>
                </div>

                <div class="column is-one-fifth is-contact">
                    <strong><i class="fas fa-envelope"></i> Contact us</strong>
                    <a href="mailto:contacto@webmarketingid.com">contacto@webmarketingid.com</a>
                </div>

                <div class="column is-one-fifth is-social">
                    <a href="https://www.facebook.com/WebMarketingID" target="_blank" class="is-facebook" title="Follow us on Facebook"><i class="fab fa-facebook"></i></a>
                    <a href="https://twitter.com/WebMarketingID" target="_blank" class="is-twitter" title="Follow us on Twitter"><i class="fab fa-twitter"></i></a>
                    <a href="#" target="_blank" class="is-instagram" title="Follow us on Instagram"><i class="fab fa-instagram"></i></a>
                </div>

            </div>
        </div>
    </div>
    
    <header class="is-header">
        <div class="container">
            <div class="columns">

                <div class="column is-one-third is-brand">
                    <a href="/en/"><img src="<?=_IMG.'web-marketing-id.png';?>"></a>
                </div>

                <div class="column is-two-thirds is-menu">
                    <ul>
                        <li><a href="/en/">Home</a></li>
                        <li><a href="/en/digital-marketing-agency">About us</a></li>
                        <li>
                            <a href="/en/digital-marketing">Services</a>
                            <ul class="is-level">
                                <li><a href="/en/seo"><i class="fas fa-search-location"></i> SEO</a></li>
                                <li><a href="/en/google-ads"><i class="fas fa-chart-line"></i> Google Ads</a></li>
                                <li><a href="/en/web-design"><i class="fas fa-laptop-code"></i> Web Design</a></li>
                                <li><a href="/en/graphic-design"><i class="fas fa-pencil-ruler"></i> Graphic Design</a></li>
                                <li><a href="/en/social-media"><i class="far fa-comments"></i> Social Media</a></li>
                            </ul>
                        </li>
                        <!--<li><a href="/clientes">Clientes</a></li>-->
                        <li><a href="/en/locations">Locations</a></li>
                        <li><a href="/en/contact">Contact us</a></li>
                        <li><a href="/blog/">Blog</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </header>