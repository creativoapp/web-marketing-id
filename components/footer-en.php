
    <footer class="is-footer">
        <div class="container">
            
            <div class="columns is-multiline is-locations">
                <div class="column is-one-fifth is-paddingless"><i class="fas fa-map-marker-alt"></i> <a href="/location-page">Aguscalientes</a></div>
                <div class="column is-one-fifth is-paddingless"><i class="fas fa-map-marker-alt"></i> <a href="/location-page">Tijuana</a></div>
                <div class="column is-one-fifth is-paddingless"><i class="fas fa-map-marker-alt"></i> <a href="/location-page">Campeche</a></div>
                <div class="column is-one-fifth is-paddingless"><i class="fas fa-map-marker-alt"></i> <a href="/location-page">Los Cabos</a></div>
                <div class="column is-one-fifth is-paddingless"><i class="fas fa-map-marker-alt"></i> <a href="/location-page">Chiapas</a></div>

                <div class="column is-one-fifth is-paddingless"><i class="fas fa-map-marker-alt"></i> <a href="/location-page">Chihuahua</a></div>
                <div class="column is-one-fifth is-paddingless"><i class="fas fa-map-marker-alt"></i> <a href="/location-page">Colima</a></div>
                <div class="column is-one-fifth is-paddingless"><i class="fas fa-map-marker-alt"></i> <a href="/location-page">Saltillo</a></div>
                <div class="column is-one-fifth is-paddingless"><i class="fas fa-map-marker-alt"></i> <a href="/location-page">Colima</a></div>
                <div class="column is-one-fifth is-paddingless"><i class="fas fa-map-marker-alt"></i> <a href="/location-page">Ciudad de México</a></div>

                <div class="column is-one-fifth is-paddingless"><i class="fas fa-map-marker-alt"></i> <a href="/location-page">Durango</a></div>
                <div class="column is-one-fifth is-paddingless"><i class="fas fa-map-marker-alt"></i> <a href="/location-page">Guadalajara</a></div>
                <div class="column is-one-fifth is-paddingless"><i class="fas fa-map-marker-alt"></i> <a href="/location-page">Pachuca</a></div>
                <div class="column is-one-fifth is-paddingless"><i class="fas fa-map-marker-alt"></i> <a href="/location-page">Morelia</a></div>
                <div class="column is-one-fifth is-paddingless"><i class="fas fa-map-marker-alt"></i> <a href="/location-page">Nayarit</a></div>
            </div>

            <div class="columns is-multiline">
                
                <div class="column is-one-third is-brand">
                    <strong>Services</strong>
                    <ul class="is-clearfix">
                        <li><a href="/en/seo"><i class="fas fa-search-location"></i> SEO</a></li>
                        <li><a href="/en/google-ads"><i class="fas fa-chart-line"></i> Google Ads</a></li>
                        <li><a href="/en/web-design"><i class="fas fa-laptop-code"></i> Web Design</a></li>
                        <li><a href="/en/graphic-design"><i class="fas fa-pencil-ruler"></i> Graphic Design</a></li>
                        <li><a href="/en/social-media"><i class="far fa-comments"></i> Social Media</a></li>
                    </ul>

                    <a href="https://www.facebook.com/WebMarketingID" target="_blank" class="is-social is-facebook" title="Follow us on Facebook"><i class="fab fa-facebook"></i></a>
                    <a href="https://twitter.com/WebMarketingID" target="_blank" class="is-social is-twitter" title="Follow us on Twitter"><i class="fab fa-twitter"></i></a>
                    <a href="#" target="_blank" class="is-social is-instagram" title="Follow us on Instagram"><i class="fab fa-instagram"></i></a>
                </div>

                <div class="column is-contact">
                    <div class="is-item">
                        <strong>Call us</strong>
                        <a href="tel:9982527702">+52 (998) 252 7702</a>
                    </div>
                </div>

                <div class="column is-contact">
                    <div class="is-item">
                        <strong>Write us</strong>
                        <a href="tel:9982527702">+52 (998) 252 7702</a>
                    </div>
                </div>

                <div class="column is-contact">
                    <div class="is-item">
                        <strong>Contact us</strong>
                        <a href="mailto:contacto@webmarketingid.com">contacto@webmarketingid.com</a>
                    </div>
                </div>

            </div>

            <div class="columns">
                <div class="column is-one-quarter is-brand">
                    <a href="/en/"><img src="<?=_IMG.'web-marketing-id-white.png';?>"></a>
                </div>
                <div class="column is-by">
                <small>All rights reserved. © Web Marketing ID <?=date('Y');?></small>
                </div>
            </div>

        </div>
    </footer>

    <script type="text/javascript" src="<?=_JS.'jquery-1.9.0.min.js';?>"></script>
    <script type="text/javascript" src="<?=_JS.'components.js';?>"></script>
	<script type="text/javascript" src="<?=_JS.'wmi.js';?>"></script>

</body>
</html>