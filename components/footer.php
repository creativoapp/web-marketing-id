
    <footer class="is-footer">
        <div class="container">
            
            <div class="columns is-multiline is-locations">
                <?php foreach($cities as $f_city) { ?>
                <div class="column is-one-fifth is-paddingless"><i class="fas fa-map-marker-alt"></i> <a href="/<?php echo $f_city->{'url'}; ?>"><?php echo $f_city->{'text'}; ?></a></div>
                <?php } ?>
            </div>

            <div class="columns is-multiline">
                
                <div class="column is-one-third is-brand">
                    <strong>Servicios</strong>
                    <ul class="is-clearfix">
                        <li><a href="/seo"><i class="fas fa-search-location"></i> Posicionamiento Web</a></li>
                        <li><a href="/publicidad-google"><i class="fas fa-chart-line"></i> Publicidad en Google</a></li>
                        <li><a href="/paginas-web"><i class="fas fa-laptop-code"></i> Páginas Web</a></li>
                        <li><a href="/diseno-grafico"><i class="fas fa-pencil-ruler"></i> Diseño Gráfico</a></li>
                        <li><a href="/redes-sociales"><i class="far fa-comments"></i> Redes Sociales</a></li>
                    </ul>

                    <a href="https://www.facebook.com/WebMarketingID" target="_blank" class="is-social is-facebook" title="Síguenos en Facebook"><i class="fab fa-facebook"></i></a>
                    <a href="https://twitter.com/WebMarketingID" target="_blank" class="is-social is-twitter" title="Síguenos en Twitter"><i class="fab fa-twitter"></i></a>
                    <a href="#" target="_blank" class="is-social is-instagram" title="Síguenos en Instagram"><i class="fab fa-instagram"></i></a>
                </div>

                <div class="column is-contact">
                    <div class="is-item">
                        <strong>Llamanos</strong>
                        <a href="tel:9982527702">+52 (998) 252 7702</a>
                    </div>
                </div>

                <div class="column is-contact">
                    <div class="is-item">
                        <strong>Escríbenos</strong>
                        <a href="tel:9982527702">+52 (998) 252 7702</a>
                    </div>
                </div>

                <div class="column is-contact">
                    <div class="is-item">
                        <strong>Contáctanos</strong>
                        <a href="mailto:contacto@webmarketingid.com">contacto@webmarketingid.com</a>
                    </div>
                </div>

            </div>

            <div class="columns">
                <div class="column is-one-quarter is-brand">
                    <a href="/"><img src="<?=_IMG.'web-marketing-id-white.png';?>"></a>
                </div>
                <div class="column is-by">
                <small>Todos los derechos reservados. © Web Marketing ID <?=date('Y');?></small>
                </div>
            </div>

        </div>
    </footer>

    <!--GOOGLE ANALYTICS-->
    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-22304550-5', 'auto');
    ga('send', 'pageview');

    </script>

    <script type="text/javascript" src="<?=_JS.'jquery-1.9.0.min.js';?>"></script>
    <script type="text/javascript" src="<?=_JS.'components.js';?>"></script>
	<script type="text/javascript" src="<?=_JS.'wmi.js';?>"></script>

</body>
</html>