<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view-servicedetail">
    
    <div class="is-header-service">
        <div class="container">
            <div class="columns">

                <div class="column is-two-thirds">
                    <h1>Posicionamiento Web México - SEO</h1>
                    <div class="is-bread">
                        <a href="/">Web Marketing ID</a>
                        <i class="fas fa-arrow-right"></i>
                        <a href="/marketing-digital">Servicios</a>
                        <i class="fas fa-arrow-right"></i>
                        <span>Posicionamiento Web - SEO</span>
                    </div>

                    <p class="is-pr-big">Estar visible en los mejores resultados de búsqueda de Google es esencial para que tu sitio web sea rentable, ya que está comprobado que los usuarios de Internet no pasan a más de la segunda página de estos resultados.</p>
                </div>
                <div class="column is-one-third">
                    <img src="<?=_IMG.'seo-ilustracion.png';?>" class="is-img-big">
                </div>

            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="columns is-multiline">

            <div class="column is-one-third">
                <img src="<?=_IMG.'mac-book.png';?>">
            </div>

            <div class="column is-two-thirds">
                <h2>Posicionamiento de tu Sitio Web en Google.</h2>
                <p class="is-pr-medium">El equipo Web Marketing ID México se encarga de realizar las actividades necesarias de SEO y posicionamiento web para que tu sitio web escale en las posiciones de Google hasta llegar a las primeras páginas.</p>
                <p class="is-pr-medium">Cuando tu sitio web haya escalado unas cuantas posiciones en Google <small>(y otros buscadores de Internet)</small> tu página web tendrá más tráfico web y por lo mismo más solicitudes de cotizaciones y ventas.</p>
            </div>

            <div class="column is-half is-listed">
                <h3>Estrategia SEO</h3>

                <p class="is-pr-medium">El Desarrollo del Posicionamiento Web de la página web de una empresa es esencial para que tenga éxito en Internet y para ello se crea una estrategia integral la cual consta de varios aspectos.</p>

                <div class="is-item">
                    <h4><i class="fas fa-ad"></i> Análisis de Palabras Claves</h4>
                    <p>Antes de crear una estrategia de Posicionamiento Web en México para tu empresa, se debe de hacer un análisis exhaustivo de las palabras claves que son importantes para tu Negocio. Para ello usamos diferentes herramientas para llegar a aquellas palabras que puedan dar un mejor rendimiento.</p>
                </div>

                <div class="is-item">
                    <h4><i class="fas fa-link"></i> Creación de Links</h4>
                    <p>Se ha dicho que lo más importante para el Posicionamiento de un Sitio Web es el contenido, y eso no es mentira, pero el contenido por sí mismo no llegará muy lejos si nadie lo encuentra.</p>
                    <p>La creación de links a un sitio web es una labor fundamental para dar a conocer el sitio web en internet, claro, hecho de una manera correcta.</p>
                    <p>La creación de links para el Posicionamiento Web se puede comparar con este ejemplo: Le pides a un diseñador que cree tus tarjetas de presentación <small>(sitio web)</small> y una vez que las tienes ya estás contento con ellas <small>(sitio web de calidad)</small>. Pero ahora tienes que hacer citas o ir a conferencias o presentaciones de tu sector para poder darte a conocer y dar tus tarjetas de presentación </p>
                </div>

                <div class="is-item">
                    <h4><i class="fab fa-wordpress-simple"></i> Creación de Contenido</h4>
                    <p>Como comentamos anteriormente, “el contenido es Rey”. Un sitio web tiene que estar bien diseñado para que sea fácil para un usuario de navegar y encontrar lo que busca, pero si este Sitio Web no tiene la información que busca el usuario estamos perdiendo prospectos.</p>
                    <p>La creación de contenido para un Sitio Web es importante para que el usuario encuentre información de valor y vea que la empresa tiene conocimientos de un tema.</p>
                </div>

                <div class="is-item">
                    <h4><i class="fas fa-anchor"></i> Medición</h4>
                    <p>Así como la creación de contenido y la creación de links son un aspecto fundamental del Posicionamiento Web, el análisis de las métricas de un Sitio Web es igual de importante, ya que ello nos da la pauta para hacer cambios necesarios en el plan de marketing y nos deja saber si lo que hacemos tiene un resultado benéfico.</p>
                </div>

            </div>

            <div class="column is-half is-listed">
                <div class="is-green">
                    <h3>Posicionamiento Web en México</h3>

                    <ul class="is-clearfix">
                    <?php foreach($cities as $city) { ?>
                    <li><a href="/<?php echo $city->{'url'}; ?>/seo"><i class="fas fa-map-marker-alt"></i> <?php echo $city->{'text'}; ?></a></li>
                    <?php } ?>
                    </ul>

                </div>


                <h4>Me interesa este servicio</h4>
                <?php include('components/form-service.php'); ?>
            </div>

        </div>
    </div>
    
</section>

<?php require __DIR__ . '/components/footer.php'; ?>