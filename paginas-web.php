<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view-servicedetail">
    
    <div class="is-header-service">
        <div class="container">
            <div class="columns">

                <div class="column is-two-thirds">
                    <h1>Páginas Web México</h1>
                    <div class="is-bread">
                        <a href="/">Web Marketing ID</a>
                        <i class="fas fa-arrow-right"></i>
                        <a href="/marketing-digital">Servicios</a>
                        <i class="fas fa-arrow-right"></i>
                        <span>Páginas Web</span>
                    </div>

                    <p class="is-pr-big">Tener una Página Web, no consiste sólo en comprarla y diseñarla; debe tener las características mercadológicas necesarias para que sea realmente rentable y que la Página Web en Sí sea otra forma de ingreso para tu compañía y no otro gasto. Web Marketing ID cuenta con profesionales en México que te pueden asesorar.</p>
                </div>
                <div class="column is-one-third">
                    <img src="<?=_IMG.'seo-ilustracion.png';?>" class="is-img-big">
                </div>

            </div>
        </div>
    </div>
    
    <div class="container">
        <div class="columns is-multiline">

            <div class="column is-one-third">
                <img src="<?=_IMG.'mac-book.png';?>">
            </div>

            <div class="column is-two-thirds">
                <h2>Diseño y Desarrollo de Sitios Web en México</h2>
                <p class="is-pr-medium">El Desarrollo de un Sitio Web o Página Web es muy importante para la Fuerza de venta en internet, forma parte del Marketing Digital sobre todo tomando en cuenta los aspectos de Visibilidad, Llamados a la acción <small>(Call to action)</small> para los usuarios, así como la fácil Navegabilidad dentro del mismo Sitio Web.</p>
                <p class="is-pr-medium">En Web Marketing ID somos expertos en la generación de prospectos en internet y para ello es importante tener un buen Diseño Web.</p>
                <p class="is-pr-medium">Contamos con diferentes opciones para tu empresa, ya sea que te proporcionemos un paquete de Diseño Web preestablecido o Diseñemos tu Sitio Web a la medida de tus necesidades.</p>
            </div>

            <div class="column is-half is-listed">
                <h3>Adquiere lo que necesitas</h3>
                
                <p class="is-pr-medium">En la mayoría de los casos se ofrecen soluciones ya existentes para reducir costos y tiempo, pero casi siempre estas soluciones contienen más de lo que necesitas o peor aún no tienen las funciones como tu las necesitas.</p>

                <div class="is-item">
                    <h4><i class="fas fa-laptop-code"></i> Páginas Web</h4>
                    <p>Las páginas web te permitiran tener presencia en internet sin importar el tipo de producto o servicio que tu brindes, tienes que estar en internet ya y una página web será una de tus mejores herramientas para exponer tu producto o servicio.</p>
                </div>

                <div class="is-item">
                    <h4><i class="fas fa-cogs"></i> Software a la medida</h4>
                    <p>Aún sigues haciendo procesos a papel o manuales, puedes automatizar sin importar cual sea tu operación con software a la medida con la ventaja inigualable de que siempre tendras exactamente lo que necesitas y como lo necesitas.</p>
                </div>

                <div class="is-item">
                    <h4><i class="fab fa-wordpress-simple"></i> Blogs</h4>
                    <p>Un blog siempre va a ser el complemento perfecto para potencializar el posicionamiento web / seo de tu marca, contar tus historias o compartir tu conocimiento con todo mundo.</p>
                </div>

            </div>

            <div class="column is-half is-listed">
                <div class="is-green">
                    <h3>Desarrollo Web en México</h3>

                    <ul class="is-clearfix">
                    <?php foreach($cities as $city) { ?>
                    <li><a href="/<?php echo $city->{'url'}; ?>/paginas-web"><i class="fas fa-map-marker-alt"></i> <?php echo $city->{'text'}; ?></a></li>
                    <?php } ?>
                    </ul>

                </div>


                <h4>Me interesa este servicio</h4>
                <?php include('components/form-service.php'); ?>
            </div>

        </div>
    </div>
   
</section>

<?php require __DIR__ . '/components/footer.php'; ?>