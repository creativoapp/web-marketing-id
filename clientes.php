<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-clients">
    <div class="container">
        <div class="columns is-multiline">

            <div class="column is-full">
                <h1>Nuestros Clientes</h1>
                <p class="is-pr-big">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
            </div>

            <div class="column is-half">
                <p class="is-pr-medium">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
            </div>
            <div class="column is-half">
                <p class="is-pr-medium">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
            </div>

        </div>
    </div>


    <!--TABS-->
    <div class="container is-tabs-clients">
        
        <div class="columns is-tabs">

            <div class="column">
                <a href="seo"><i class="fas fa-search-location"></i> Posicionamiento Web</a>
            </div>

            <div class="column">
                <a href="campaigns"><i class="fas fa-chart-line"></i> Gestión de Campañas</a>
            </div>

            <div class="column">
                <a href="web"><i class="fas fa-laptop-code"></i> Desarrollo Web</a>
            </div>

            <div class="column">
                <a href="graphic"><i class="fas fa-pencil-ruler"></i> Diseño Gráfico</a>
            </div>

            <div class="column">
                <a href="social"><i class="far fa-comments"></i> Redes Sociales</a>
            </div>

        </div>


        <div class="columns is-multiline is-body">

            <div class="column is-one-quarter is-web is-campaigns">
                <img src="<?=_IMG.'projects/desarrollo.jpg';?>" class="is-img-spaced">
                <h2>Sundec Decoración</h2>
                <a href="#">Desarrollo Web</a>, <a href="#">Gestión de Camapañas</a>
            </div>

            <div class="column is-one-quarter is-seo">
                <img src="<?=_IMG.'projects/seo.jpg';?>" class="is-img-spaced">
                <h2>DG&H</h2>
                <a href="#">Posicionamiento Web</a>
            </div>

            <div class="column is-one-quarter is-campaigns is-graphic">
                <img src="<?=_IMG.'projects/desarrollo.jpg';?>" class="is-img-spaced">
                <h2>America Transfers</h2>
                <a href="#">Gestión de Campañas</a>, <a href="#">Diseño Gráfico</a>
            </div>

            <div class="column is-one-quarter is-social">
                <img src="<?=_IMG.'projects/seo.jpg';?>" class="is-img-spaced">
                <h2>Acamaya Reef Cabañas</h2>
                <a href="#">Redes Sociales</a>
            </div>

        </div>

    </div>

</section>

<?php require __DIR__ . '/components/footer.php'; ?>