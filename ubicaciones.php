<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-locations">
    <div class="container">
        <div class="columns is-multiline">

            <div class="column is-full">
                <h1>Marketing Digital en México</h1>
            </div>

            <div class="column is-half">
                <p class="is-pr-medium">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
            </div>
            <div class="column is-half">
                <p class="is-pr-medium">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
            </div>

        </div>
    </div>

    
    <div class="is-cities">
        <div class="container">
            <div class="columns is-multiline">

                <?php foreach($cities as $city) { ?>
                <div class="column is-one-quarter">
                    <h2><a href="/<?php echo $city->{'url'}; ?>"><?php echo $city->{'text'}; ?></a></h2>
                    <ul>
                        <li><a href="/<?php echo $city->{'url'}; ?>/seo"><i class="fas fa-search-location"></i> Posicionamiento Web</a></li>
                        <li><a href="/<?php echo $city->{'url'}; ?>/publicidad-google"><i class="fas fa-chart-line"></i> Publicidad en Google</a></li>
                        <li><a href="/<?php echo $city->{'url'}; ?>/paginas-web"><i class="fas fa-laptop-code"></i> Páginas Web</a></li>
                        <li><a href="/<?php echo $city->{'url'}; ?>/diseno-grafico"><i class="fas fa-pencil-ruler"></i> Diseño Gráfico</a></li>
                        <li><a href="/<?php echo $city->{'url'}; ?>/redes-sociales"><i class="far fa-comments"></i> Redes Sociales</a></li>
                    </ul>
                </div>
                <?php } ?>

            
            </div>
        </div>
    </div>

</section>

<?php require __DIR__ . '/components/footer.php'; ?>