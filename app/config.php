<?php

//Constants
define('_IMG', '/assets/img/');
define('_CSS', '/assets/css/');
define('_JS', '/assets/js/');

//Global Vars
$routeUri = $_SERVER['PHP_SELF'];
$pageUri = basename($_SERVER['SCRIPT_NAME']);
$cityUriExtend = '';
$cv = '1.0.0';

$pageLayout = 'default';
switch($routeUri) {
    case '/city-page.php':
        $pageLayout = 'city';
        break;
    case '/city-about-page.php':
        $pageLayout = 'city-about';
        break;
    case '/city-service-page.php':
        $pageLayout = 'city-service';
        break;
    case '/city-seo-page.php':
    case '/city-ads-page.php':
    case '/city-web-page.php':
    case '/city-design-page.php':
    case '/city-social-page.php':
        $pageLayout = 'city-service-detail';
        break;
    case '/city-contact-page.php':
        $pageLayout = 'city-contact';
        break;
}

//Dinamic Content
$sliderCaptions = ['seo' => '<h3>Posicionamiento <span>Web</span></h3>
                             <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>',
                    'dev' => '<h3>Desarrollo <span>Web</span></h3>
                             <p>Es un hecho establecido hace demasiado tiempo que un lector se distraerá con el contenido del texto de un sitio mientras que mira su diseño.</p>'];


//LoadModels
require('models/mCities.php');
require('models/mSeo.php');

//City Rules
$citiesObj = new CitiesModel();
$cities = $citiesObj->citiesList('mx');
$city_page = array('url' => null, 'name' => null, 'nav' => '');

// City Page
if($pageLayout == 'city') {
    $city_arg = explode('=', $_SERVER['QUERY_STRING']);
    $city_page['url'] = $city_arg[1];

    if(isset($cities->{''.$city_arg[1].''})) {
        $city_page['name'] = $cities->{''.$city_arg[1].''}->{'text'};
        $cityUriExtend = $cities->{''.$city_arg[1].''}->{'url'};
    }

}

// City -> {Section}
if($pageLayout != 'default' && $pageLayout != 'city') {
    $city_arg = explode('=', $_SERVER['QUERY_STRING']);
    $city_url = explode('/', $city_arg[1]);
    $city_page['url'] = $city_url[0];

    if(isset($cities->{''.$city_url[0].''})) {
        $city_page['name'] = $cities->{''.$city_url[0].''}->{'text'};
        $cityUriExtend = $cities->{''.$city_url[0].''}->{'url'};
    }
}

//Seo Rules
$seoObj = new SeoModel();