<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-home is-view-city">

    <!--SERVICIOS-->
    <div class="container">
        <div class="columns is-multiline">

            <div class="column is-full is-overview">
                <h1>Agencia de Marketing Digital en <?=$city_page['name'];?></h1>
                <div class="is-bread">
                    <a href="/">Web Marketing ID</a>
                    <i class="fas fa-arrow-right"></i>
                    <a href="/ubicaciones">Ubicaciones</a>
                    <i class="fas fa-arrow-right"></i>
                    <span><?=$city_page['name'];?></span>
                </div>
                <p class="is-pr-big">Somos una Agencia de Marketing digital en <?=$city_page['name'];?>. Nos enfocamos en crear, desarrollar e implementar soluciones integrales y eficientes que se orienten a los objetivos de tu empresa.</p>
                <p class="is-pr-medium">Analizamos cuáles son las necesidades de tu empresa y con ello creamos un plan para el desarrollo de tu marca en internet, ya sea con la creación de un nuevo Sitio Web o el rediseño de uno ya existente, así como el desarrollo del Posicionamiento Web del mismo para estar en los mejores lugares de los resultados de búsqueda de Google.</p>
            </div>

            <div class="column is-one-third is-card">
                <div>
                    <h3><i class="fas fa-search-location"></i> <a href="/<?=$city_page['url'];?>/seo">SEO / Posicionamiento web <?=$city_page['name'];?></a></h3>
                    <p>Con el Posicionamiento Web o SEO <small>(Search Engine Optimization)</small> llevamos a tu Página Web a las mejores posiciones en los resultados de búsqueda <small>(Google)</small> a través de actividades de Marketing Digital.</p>
                </div>
            </div>

            <div class="column is-one-third is-card">
                <div>
                    <h3><i class="fas fa-chart-line"></i> <a href="/<?=$city_page['url'];?>/publicidad-google">Pago por clic / Google Ads <?=$city_page['name'];?></a></h3>
                    <p>Lleva tu Sitio Web a los mejores resultados de Google rápido y fácil con El Pago Por Clic de Google Ads haciendo que las ventas incrementen rápidamente.</p>
                </div>
            </div>

            <div class="column is-one-third is-card">
                <div>
                    <h3><i class="fas fa-laptop-code"></i> <a href="/<?=$city_page['url'];?>/paginas-web">Diseño web <?=$city_page['name'];?></a></h3>
                    <p>Diseña tu Sitio Web para ganar! El diseño y desarrollo de Páginas Web es muy Importante y sobretodo es el primer paso para tener presencia en Internet. Diseñamos Páginas Web con las características necesarias para su Posicionamiento Web y la generación de Prospectos.</p>
                </div>
            </div>

            <div class="column is-one-third is-card">
                <div>
                    <h3><i class="fas fa-pencil-ruler"></i> <a href="/<?=$city_page['url'];?>/diseno-grafico">Diseño Gráfico <?=$city_page['name'];?></a></h3>
                    <p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500.</p>
                </div>
            </div>

            <div class="column is-one-third is-card">
                <div>
                    <h3><i class="far fa-comments"></i> <a href="/<?=$city_page['url'];?>/redes-sociales">Redes Sociales <?=$city_page['name'];?></a></h3>
                    <p>Hoy en día es Necesario tener presencia en todos los medios posibles, y las Redes Sociales están a la vanguardia. Ayudamos con la creación del perfil de la empresa en Redes Sociales, así como su administración y creación de contenido.</p>
                </div>
            </div>

        </div>
    </div>

    <!--CONTACTO-->
    <div class="is-we-contact">
        <div class="container">
            <div class="columns is-multiline">

                <div class="column is-full">
                    <h3>¿Tienes un Proyecto?</h3>
                    <p>Hagamoslo ya!. Danos tu información para contactarte y ayudarte con tu proyecto.</p>

                    <div class="is-custom">
                        <input type="text" id="h_inpEmail" class="is-first" placeholder="Escribe aquí tu correo">
                        <input type="text" id="h_inpNumber" placeholder="Escribe aquí tu telefono">
                        <button class="is-last"><i class="fas fa-fax"></i> CONTÁCTAME</button>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <!--CIUDADES-->
    <div class="container is-locations">
        <div class="columns">

            <div class="column is-two-fifths">
                <img src="<?=_IMG.'location-ilustracion.png';?>">
            </div>
            <div class="column">
                <h3>Marketing Digital en México</h3>
                <p class="is-pr-medium">Web Marketing ID te lleva servicios de Marketing Digital a los principales destinos de México y constantemente abriendo nuevas ciudades.</p>

                <ul class="is-clearfix">
                    <?php foreach($cities as $city) { ?>
                    <li><a href="/<?php echo $city->{'url'}; ?>"><i class="fas fa-map-marker-alt"></i> <?php echo $city->{'text'}; ?></a></li>
                    <?php } ?>
                </ul>

            </div>

        </div>
    </div>

</section>

<?php require __DIR__ . '/components/footer.php'; ?>