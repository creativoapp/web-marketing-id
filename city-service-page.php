<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-services">
    <div class="container">
        <div class="columns is-multiline is-variable is-8">

            <div class="column is-one-third">
                <h1>Servicios de <span>Marketing Digital</span><span>en <?=$city_page['name'];?></span></h1>
                <p class="is-pr-big">El Marketing Digital hoy en día es primordial para todas las empresas, ya que cada vez más consumidores están inmersos en la Red buscando cosas que necesitan porque se les hace más fácil que salir o están distrayéndose en las Redes Sociales, en donde hablan de sus marcas favoritas y los lugares o servicios que les han gustado.</p>
            </div>

            <div class="column is-one-third is-card">
                <div>
                    <a href="/<?=$city_page['url'];?>/seo"><img src="<?=_IMG.'seo-ilustracion.png';?>" class="is-img-big"></a>
                    <h2><a href="/<?=$city_page['url'];?>/seo">Posicionamiento Web</a></h2>
                    <p>Con el Posicionamiento Web o SEO <small>(Search Engine Optimization)</small> llevamos a tu Página Web a las mejores posiciones en los resultados de búsqueda <small>(Google)</small> a través de actividades de Marketing Digital.</p>
                    <a href="/<?=$city_page['url'];?>/seo" class="is-link">Saber más <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>

            <div class="column is-one-third is-card">
                <div class="is-green">
                    <a href="/<?=$city_page['url'];?>/publicidad-google"><img src="<?=_IMG.'dev-ilustracion.png';?>" class="is-img-big"></a>
                    <h2><a href="/<?=$city_page['url'];?>/publicidad-google">Publicidad en Google</a></h2>
                    <p>Lleva tu Sitio Web a los mejores resultados de Google rápido y fácil con El Pago Por Clic de Google Ads haciendo que las ventas incrementen rápidamente.</p>
                    <a href="/<?=$city_page['url'];?>/publicidad-google" class="is-link">Saber más <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>

            <div class="column is-one-third is-card">
                <div class="is-blue">
                    <a href="/<?=$city_page['url'];?>/paginas-web"><img src="<?=_IMG.'seo-ilustracion.png';?>" class="is-img-big"></a>
                    <h2><a href="/<?=$city_page['url'];?>/paginas-web">Páginas Web</a></h2>
                    <p>El diseño y desarrollo de Páginas Web es algo Importante y sobretodo es el primer paso para tener presencia en Internet. Diseñamos Páginas Web con las características necesarias para su Posicionamiento.</p>
                    <a href="/<?=$city_page['url'];?>/paginas-web" class="is-link">Saber más <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>

            <div class="column is-one-third is-card">
                <div class="is-bluelite">
                    <a href="/<?=$city_page['url'];?>/diseno-grafico"><img src="<?=_IMG.'dev-ilustracion.png';?>" class="is-img-big"></a>
                    <h2><a href="/<?=$city_page['url'];?>/diseno-grafico">Diseño Gráfico</a></h2>
                    <p>Diseña tu Sitio Web para ganar! El diseño y desarrollo de Páginas Web es muy Importante y sobretodo es el primer paso para tener presencia en Internet. Diseñamos Páginas Web con las características necesarias para su Posicionamiento Web y la generación de Prospectos.</p>
                    <a href="/<?=$city_page['url'];?>/diseno-grafico" class="is-link">Saber más <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>

            <div class="column is-one-third is-card">
                <div class="is-purple">
                    <a href="/<?=$city_page['url'];?>/redes-sociales"><img src="<?=_IMG.'seo-ilustracion.png';?>" class="is-img-big"></a>
                    <h2><a href="/<?=$city_page['url'];?>/redes-sociales">Redes Sociales</a></h2>
                    <p>Hoy en día es Necesario tener presencia en todos los medios posibles, y las Redes Sociales están a la vanguardia. Ayudamos con la creación del perfil de la empresa en Redes Sociales, así como su administración y creación de contenido.</p>
                    <a href="/<?=$city_page['url'];?>/redes-sociales" class="is-link">Saber más <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>

        </div>
    </div>

</section>

<?php require __DIR__ . '/components/footer.php'; ?>