<?php require __DIR__ . '/components/header.php'; ?>

<section class="is-view is-view-contact">
    <div class="container">
        <div class="columns is-multiline">

            <div class="column is-full">
                <h1>Marketing Digital en México</h1>
                <p class="is-pr-big">Somos la empresa más grande de Marketing Digital en México, contamos con especialistas SEO y SEM altamente capacitados, así como expertos en estrategias digitales y Content Marketing.</p>
            </div>

        </div>
    </div>

    
    <div class="is-contact">
        <div class="container">
            <div class="columns">
                
                <div class="column is-one-quarter">
                    <strong><i class="fas fa-map-marker-alt"></i> Oficinas</strong>
                    <p>Cancún, Quintana Roo. C.P. 77506<br>
                    Av. La Luna 4B, 48</p>
                </div>

                <div class="column is-one-quarter">
                    <strong><i class="fas fa-envelope-open-text"></i> Soporte</strong>
                    <a href="mailto:info@webmarketingid.com">info@webmarketingid.com</a>
                </div>

                <div class="column is-one-quarter">
                    <strong><i class="fas fa-fax"></i> Llamanos</strong>
                    <a href="tel:9982527702">(998) 252 7702</a>
                </div>

                <div class="column is-one-quarter">
                    <strong><i class="far fa-comments"></i> Charlemos</strong>
                    <span>Lun - Vie 9AM / 6PM</span>
                </div>

            </div>
        </div>
    </div>

    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14884.676290272238!2d-86.844207!3d21.1456688!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa3dbb426d753c215!2sWeb%20Marketing%20ID%20-%20Agencia%20Digital%20en%20Canc%C3%BAn!5e0!3m2!1ses!2smx!4v1589642467565!5m2!1ses!2smx" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

    <div class="container">
        <div class="columns is-variable is-5">
                
            <div class="column is-half">
                <form name="formGlobalService" method="post" action="" class="columns is-multiline">
                    <fieldset class="column is-full">
                        <label for="inpName">Nombre</label>
                        <input type="text" name="inpName" id="inpName">
                    </fieldset>
                    <fieldset class="column is-half">
                        <label for="inpEmail">Email</label>
                        <input type="text" name="inpEmail" id="inpEmail">
                    </fieldset>
                    <fieldset class="column is-half">
                        <label for="inpNumber">Teléfono</label>
                        <input type="text" name="inpNumber" id="inpNumber">
                    </fieldset>
                    <fieldset class="column is-half">
                        <label for="inpInteresting">Estoy interesado en</label>
                        <select name="inpInteresting" id="inpInteresting">
                            <option value="0">-</option>
                            <option value="Seo">Posicionamiento Web</option>
                            <option value="Campaign">Gestión de Camapañas</option>
                            <option value="Web">Desarrollo Web</option>
                            <option value="Diseno">Diseño Gráfico</option>
                            <option value="Redes">Redes Sociales</option>
                            <option value="Otro">Otros</option>
                        </select>
                    </fieldset>
                    <fieldset class="column is-half">
                        <label for="inpKnowus">Como supiste de nosotros</label>
                        <select name="inpKnowus" id="inpKnowus">
                            <option value="Google">Google</option>
                            <option value="Facebook">Facebook</option>
                            <option value="Campaign">Anuncio</option>
                            <option value="Friend">Un amigo</option>
                            <option value="Otro">Otros</option>
                        </select>
                    </fieldset>
                    <fieldset class="column is-full">
                        <label for="inpComments">Estoy interesado en</label>
                        <textarea name="inpComments" id="inpComments" rows="5"></textarea>
                    </fieldset>
                    <fieldset class="column is-full">
                        <button id="btnSendGlobalForm">CONTACTAR <i class="fas fa-arrow-right"></i></button>
                    </fieldset>
                </form>
            </div>

            <div class="column is-half">
                <h3>Preguntas y respuestas</h3>

                <div class="is-questions">
                    <div class="is-item">
                        <div class="is-question">
                            <strong>¿Estan fisicamente en todas las ciudades?</strong>
                            <i class="fas fa-chevron-down"></i>
                        </div>
                        <div class="is-answer is-default">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                    </div>
                    <div class="is-item">
                        <div class="is-question">
                            <strong>¿Estan fisicamente en todas las ciudades?</strong>
                            <i class="fas fa-chevron-down"></i>
                        </div>
                        <div class="is-answer">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                    </div>
                    <div class="is-item">
                        <div class="is-question">
                            <strong>¿Estan fisicamente en todas las ciudades?</strong>
                            <i class="fas fa-chevron-down"></i>
                        </div>
                        <div class="is-answer">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                    </div>
                    <div class="is-item">
                        <div class="is-question">
                            <strong>¿Estan fisicamente en todas las ciudades?</strong>
                            <i class="fas fa-chevron-down"></i>
                        </div>
                        <div class="is-answer">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                    </div>
                    <div class="is-item">
                        <div class="is-question">
                            <strong>¿Estan fisicamente en todas las ciudades?</strong>
                            <i class="fas fa-chevron-down"></i>
                        </div>
                        <div class="is-answer">
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    
</section>

<?php require __DIR__ . '/components/footer.php'; ?>